const express = require('express')
const cors = require('cors');
const {newChord, categoryAZ, byArtist, bySong } = require('./routes/index')

const app = express();
const PORT = 5000

app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({ extended: true })
);

app.get("/", function (req, res) {
    res.status(200).json("Server musikIN is up and running..");
});

app.use('/new', newChord)
app.use('/category', categoryAZ)
app.use('/artist', byArtist)
app.use('/song', bySong)
// app.use('/lyrics', byLyrics)

app.listen(PORT, () => console.log('server running on port 5000...'));