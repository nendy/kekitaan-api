const newChord = require('./chord')
const categoryAZ = require('./category')
const byArtist = require('./artist')
const bySong = require('./song')
// const byLyrics = require('./lyrics')

module.exports = {
	newChord,
	categoryAZ,
	byArtist,
	bySong,
	// byLyrics
};